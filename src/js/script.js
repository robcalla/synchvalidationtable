$(document).ready(function () {
    console.log("Welcome.");
    var refresh = (qs('forceRefresh') == 'true');
    $.ajax({
        url: `https://validation.services.synchronicity-iot.eu/aggregatedDataModels?forceRefresh=${refresh}`,
        type: 'GET',
        success: function (response) {

            var trHTML = '';
            var tHeaders = "";
            var cityTot = [];
            var headerOK = false;

            $.each(response, function (i, item) {

                if (!headerOK) {
                    tHeaders += '<td style="width:15%" class="align-middle">'+
                        '<span style="font-size:12px;">Validated Entities Rate</span>'+
                        '<img src="/table/gradient.png" class="img-fluid" alt="gradient">'+
                        '<div class="d-flex justify-content-between">'+
                        '<span style="font-size:10px;">0%</span>'+
                        '<span style="font-size:10px;">50%</span>'+
                        '<span style="font-size:10px;">100%</span>'+
                        '</div>'+
                    '</td>';
                    var tdSize = Math.floor(85 / (item.cityValues.length + 1));
                    $.each(item.cityValues, function (i, city) {
                        tHeaders += '<td class="rotate" style="width:' + tdSize + '%" data-toggle="tooltip" data-placement="bottom" title="Updated: ' + formatDate(city.lastUpdate) + '"><div><span class="strong">' + city.city + '</span></div></td>';
                    });
                    headerOK = true;
                    tHeaders += '<td class="rotate" style="width:' + (tdSize + (85 - tdSize * (item.cityValues.length + 1))) + '%;"><div><span class="strong">Total</span></div></td>';
                }

                trHTML += '<tr><td><a href=' + (item.docRef.includes('http') ? item.docRef : item.schema) + ' target="_blank">' + item.dataModel + '</a></td>';
                var globalTot = 0;

                $.each(item.cityValues, function (j, city) {
                    if (i == 0) {
                        cityTot[j] = city.total;
                    } else {
                        cityTot[j] += city.total;
                    }

                    globalTot += city.total;
                    if (city.total == 0)
                        trHTML += '<td></td>';
                    else {
                        let percentage = (city.success / city.total) * 100;
                        let tooltipText = "<ul class='list-group' style='background-color:black'><li class='list-group-item li-tooltip'> Total: " + city.total + "</li> <li class='list-group-item li-tooltip'>Validated: " + city.success + " </li><li class='list-group-item li-tooltip'> " + parseFloat(percentage).toFixed(2) + " %</li></ul>";
                        trHTML += "<td style='background-color:" + getColor(percentage) + "' data-toggle='tooltip' data-placement='bottom' title=\"" + tooltipText + "\" data-html='true'></td>"
                    }
                });
                trHTML += '<td>' + globalTot + '</td></tr>';

            });
            trHTML += "<tr><td>City Total</td>";
            var globalTot = 0;
            $.each(cityTot, function (i, e) {
                globalTot += e;
                trHTML += "<td>" + e + "</td>";
            });
            trHTML += "<td>" + globalTot + "</td><tr>";

            $('ul').addClass('list-group');
            $('ul li').addClass('list-group-item li-tooltip');
            $('#tableHeaders').append(tHeaders);
            $('#tableBody').append(trHTML);

            $('[data-toggle="tooltip"]').tooltip()
        }
    });

    function formatDate(d){
        return new Date(d).toLocaleString();
    }

    function qs(key) {
        key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
        var match = location.search.match(new RegExp("[?&]" + key + "=([^&]+)(&|$)"));
        return match && decodeURIComponent(match[1].replace(/\+/g, " "));
    }

    function getColor(percentage) {
        
        if((percentage>=0 && percentage<10)){
            return '#FF7B00';
        }else if((percentage>=10 && percentage<20)){
            return '#FF9E00';
        }else if((percentage>=20 && percentage<30)){
            return '#FFC100';
        }else if((percentage>=30 && percentage<40)){
            return '#FFE400';
        }else if((percentage>=40 && percentage<50)){
            return '#F7FF00';
        }else if((percentage>=50 && percentage<60)){
            return '#D4FF00';
        }else if((percentage>=60 && percentage<70)){
            return '#B0FF00';
        }else if((percentage>=70 && percentage<80)){
            return '#8DFF00';
        }else if((percentage>=80 && percentage<90)){
            return '#6AFF00';
        }else if((percentage>=90 && percentage<=100)){
            return '#4caf50';
        }else{
            return "#fff";
        }

    };



});